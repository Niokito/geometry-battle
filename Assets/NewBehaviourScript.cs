﻿using UnityEngine;
using System.Collections;

public class NewBehaviourScript : MonoBehaviour
{
    [SerializeField]
    private float speed = 100f;
    void Update()
    {
        transform.position = gameObject.transform.position;

        GameObject child;
        if (Input.GetKey(KeyCode.UpArrow))
        {
            //while (transform.eulerAngles.z < 90)
            //print(transform.eulerAngles.z);
            child = GameObject.Find("LeftPivot");
            gameObject.transform.RotateAround(child.transform.position, Vector3.forward, Time.deltaTime * this.speed);
            child.transform.Translate(Vector3.right, Space.World);
            print(child.transform.position);
        }
    }
}
//using UnityEngine;
//using System.Collections;

//public class NewBehaviourScript : MonoBehaviour
//{
//    public float speed = 1;
//    private GameObject endRotation;
//    void Start()
//    {
//        endRotation = new GameObject();
//    }

//    void Update()
//    {
//        if (Input.GetKeyDown(KeyCode.RightArrow))
//        {
//            //while(transform.eulerAngles.z < 90)
//              transform.Rotate(Vector3.forward, 90, Space.World);
//        }
//        if (Input.GetKeyDown(KeyCode.LeftArrow))
//        {
//            endRotation.transform.Rotate(Vector3.forward, -90, Space.World);
//        }

//        if (Input.GetKeyDown("a"))
//        {
//            endRotation.transform.Rotate(Vector3.up, 90, Space.World);
//        }
//        if (Input.GetKeyDown("d"))
//        {
//            endRotation.transform.Rotate(Vector3.up, -90, Space.World);
//        }
//        if (Input.GetKeyDown("w"))
//        {
//            endRotation.transform.Rotate(Vector3.left, 90, Space.World);
//        }
//        if (Input.GetKeyDown("s"))
//        {
//            endRotation.transform.Rotate(Vector3.left, -90, Space.World);
//        }

//        this.transform.rotation = Quaternion.Lerp(this.transform.rotation, endRotation.transform.rotation, Time.deltaTime * speed);
//    }
//}
