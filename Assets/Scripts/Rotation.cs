﻿using UnityEngine;

public class Rotation : MonoBehaviour {

    void Start () {

    }

    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            float horizontal = Input.GetAxis("Mouse X") * 500;
            float vertical = Input.GetAxis("Mouse Y") * 500;
            transform.Rotate(0, horizontal * Time.deltaTime, 0);

            float z = transform.eulerAngles.z;
            transform.Rotate(0, 0, -z);
        }
    }
}
