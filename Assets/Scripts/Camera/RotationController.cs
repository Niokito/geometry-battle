﻿using UnityEngine;

public class RotationController : MonoBehaviour
{
    [RangeAttribute(1, 1000)]
    public int LookSensitivity = 1000;

    public GameObject target;

    public Vector3 offset;

    void Start()
    {
        this.offset = this.target.GetComponent<Transform>().position - this.GetComponent<Transform>().position;
    }

    void LateUpdate()
    {
        float desiredAngle = this.target.GetComponent<Transform>().eulerAngles.y;
        Quaternion rotation = Quaternion.Euler(0, desiredAngle, 0);
        transform.position = target.transform.position - (rotation * offset);
        transform.LookAt(this.target.transform);
    }
}