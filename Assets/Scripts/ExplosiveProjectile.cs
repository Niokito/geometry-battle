using UnityEngine;

public class ExplosiveProjectile : Projectile
{

    [RangeAttribute(1, 10)]
    public float ExpolosionRadius = 5;

    public new delegate void onCollisionDelegate(Projectile self, Collision collision, Collider[] overlaping);

    public new event onCollisionDelegate onCollision;

    protected override void OnCollisionEnter(Collision collision)
    {
        if (this.isFired)
        {
            Collider[] colliders = Physics.OverlapSphere(collision.transform.position, this.ExpolosionRadius);

            if (this.onCollision != null)
            {
                this.onCollision(this, collision, colliders);
            }
        }
    }
}