﻿using UnityEngine;
using System.Collections;

public class CubeMovementScript : MonoBehaviour {

    private new Rigidbody rigidbody;

    public bool isDead = false;

    private GameObject LineOfDefence;

    void Start () {
        this.rigidbody = GetComponent<Rigidbody>();
        this.LineOfDefence = GameObject.Find("LineOfDefence");
        StartCoroutine("movementRoutine");
    }

    void Update () {

    }

    private IEnumerator movementRoutine() {
        while (!isDead)
        {
            Vector3 force = Vector3.back * 4f;
            this.rigidbody.AddForce(force, ForceMode.Impulse);

            yield return new WaitForSeconds(2f);
        }
    }

    void LateUpdate()
    {
        if (this.GetComponent<Transform>().position.z <= this.LineOfDefence.GetComponent<Transform>().position.z && !this.isDead)
        {
            GameObject.Find("ShootingController").GetComponent<ShootingController>().health--;
            GameObject.Find("ShootingController").GetComponent<ShootingController>().healthText.text =
                "Health " + GameObject.Find("ShootingController").GetComponent<ShootingController>().health.ToString();
            Destroy(this.gameObject);
        }
    }
}
