﻿using UnityEngine;
using System.Collections;

public class ShieldScript : MonoBehaviour
{
    private bool buff;
    private int shield = 0;
    private Color color;

    void Start()
    {
        this.color = GetComponent<Renderer>().material.color;
        GameObject.FindGameObjectWithTag("Buff").GetComponent<BuffScript>().onCollected += onBuffCollect;
    }   

    private void onBuffCollect(Collider collider)
    {
        this.shield += 20;
        color.a = this.shield * 0.01f;
        GetComponent<Renderer>().material.color = color;
    }
}