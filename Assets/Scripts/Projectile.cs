﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    public delegate void BasicEvent(Projectile self);

    public delegate void onCollisionDelegate(Projectile self, Collision collider);

    public event BasicEvent onMouseDown;

    public event BasicEvent onMouseUp;

    public event onCollisionDelegate onCollision;

    public bool isFired = false;

    public int colorCode;

    private void OnMouseDown()
    {
        if (this.onMouseDown != null)
        {
            this.onMouseDown(this);
        }
    }

    private void OnMouseUp()
    {
        if (this.onMouseUp != null)
        {
            this.onMouseUp(this);
        }
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (this.onCollision != null) {
            this.onCollision(this, collision);
        }
    }
}