﻿using UnityEngine;
using System.Collections;

public class BuffScript : MonoBehaviour
{
    public delegate void onCollectedDelegate(Collider collision);

    public event onCollectedDelegate onCollected;

    public Transform spawnPoint;
    private bool hasRespawned = false;

    void Start()
    {
        StartCoroutine("Dissapear");
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (this.onCollected != null && collision.transform.gameObject.tag == "Projectile")
        {
            this.onCollected(collision);
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<BoxCollider>().enabled = false;
        }
    }

    private IEnumerator Dissapear()
    {
        while (true)
        {
            transform.LookAt(spawnPoint);
            transform.eulerAngles = new Vector3(90f, transform.rotation.eulerAngles.y, 0);
            GetComponent<MeshRenderer>().enabled = true;
            yield return new WaitForSeconds(5f);
            GetComponent<BoxCollider>().enabled = false;
            GetComponent<MeshRenderer>().enabled = false;
            yield return new WaitForSeconds(10f);
        }
    }
}
