﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour
{
    [SerializeField]
    private Button exitText;
    [SerializeField]
    private Button backToMainMenuText;

    void Start()
    {
        this.exitText = this.exitText.GetComponent<Button>();
        this.backToMainMenuText = this.backToMainMenuText.GetComponent<Button>();
    }

    public void BackToMainMenu()
    {
        Application.LoadLevel(0);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
