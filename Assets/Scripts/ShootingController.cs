﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShootingController : MonoBehaviour
{
    public Projectile BasicProjectile;

    public ExplosiveProjectile explosiveProjectile;

    public Transform ProjectileSpawnLocation;

    public AudioClip destroySound;

    public AudioClip impactSound;

    public Material[] materials;

    private Projectile CurrentProjectile;

    private Vector3 mouseStartPosition;

    private int projectileLifetime = 8;

    private float respawnTime = 1.5f;

    private CubeGenerator CubeGenerator;

    public int health = 5;

    public Text scoreText;

    public Text healthText;

    private int scoreCount = 0;

    private void Start()
    {
        scoreText.text = "Score  " + scoreCount.ToString();
        this.CubeGenerator = GameObject.FindGameObjectWithTag("CubeGenerator").GetComponent<CubeGenerator>();
        this.ProjectileSpawnLocation = GameObject.FindGameObjectWithTag("SphereSpawnLocation").transform;
        this.CurrentProjectile = this.CreateBasicProjectile();
    }

    private void Update()
    {
        GameOver();
    }

    private Projectile CreateBasicProjectile()
    {
        int color = this.CalculateColor();

        BasicProjectile.GetComponent<MeshRenderer>().material = materials[color];
        BasicProjectile.colorCode = color;

        Projectile newProjectile = Instantiate(this.BasicProjectile, this.ProjectileSpawnLocation.position, this.ProjectileSpawnLocation.rotation) as Projectile;
        newProjectile.onMouseDown += this.onProjectileMouseDown;
        newProjectile.onMouseUp += this.onProjectileMouseUp;

        return newProjectile;
    }

    private void Reload()
    {
        this.CurrentProjectile = CreateBasicProjectile();
        GameObject.Find("ExplosiveShotBtn").GetComponent<Button>().interactable = true;
    }

    public void LoadExplosive()
    {
        this.CurrentProjectile.onMouseDown -= this.onProjectileMouseDown;
        this.CurrentProjectile.onMouseUp -= this.onProjectileMouseUp;

        Destroy(this.CurrentProjectile.gameObject);
        Destroy(this.CurrentProjectile);

        var newProjectile = Instantiate(this.explosiveProjectile, this.ProjectileSpawnLocation.position, this.ProjectileSpawnLocation.rotation) as ExplosiveProjectile;

        newProjectile.onMouseDown += this.onProjectileMouseDown;
        newProjectile.onMouseUp += this.onProjectileMouseUp;
        newProjectile.onCollision += this.onProjectileExplosion;

        this.CurrentProjectile = newProjectile;
    }

    private void DestroyEnemy(GameObject enemy)
    {
        enemy.GetComponent<CubeMovementScript>().isDead = true;
        var particleSystem = enemy.GetComponentInChildren<ParticleSystem>();
        var particles = new ParticleSystem.Particle[particleSystem.maxParticles];

        particleSystem.GetParticles(particles);
        particleSystem.Emit(30);

        enemy.GetComponent<Renderer>().enabled = false;

        AudioSource.PlayClipAtPoint(this.destroySound, this.ProjectileSpawnLocation.transform.position);

        Destroy(enemy.GetComponent<Collider>());
        Destroy(enemy, 5);
        this.scoreCount += 20;
        scoreText.text = "Score - " + scoreCount.ToString();
    }

    private int CalculateColor()
    {
        float total = this.CubeGenerator.activeCubes[0] + this.CubeGenerator.activeCubes[1] + this.CubeGenerator.activeCubes[2];

        for (int i = 0; i < 3; i++)
        {
            if (total == 0)
            {
                continue;
            }

            float chance = 100 * (this.CubeGenerator.activeCubes[i] / total);

            int randomNumber = UnityEngine.Random.Range(0, 100);

            if (randomNumber < chance) {
                return i;
            }
        }

        return UnityEngine.Random.Range(0, 3);
    }

    private void onProjectileCollision(Projectile projectile, Collision collision)
    {
        AudioSource.PlayClipAtPoint(this.impactSound, collision.transform.position);
        if (collision.gameObject.tag == "Enemy" && this.canBeDestroyed(projectile.GetComponent<MeshRenderer>(), collision.collider.gameObject.GetComponent<MeshRenderer>()))
        {
            if (collision.gameObject.GetComponent<MeshRenderer>().material.name == "FogDebuff (Instance)")
            {
                StartCoroutine("StartFog");
            }

            this.CubeGenerator.activeCubes[projectile.colorCode] = Mathf.Clamp(this.CubeGenerator.activeCubes[projectile.colorCode] - 1, 0, this.CubeGenerator.activeCubes[projectile.colorCode]);

            this.DestroyEnemy(collision.collider.gameObject);
            Destroy(projectile.gameObject);
        }
    }

    private void onProjectileExplosion(Projectile projectile, Collision collision, Collider[] overlaping)
    {
        projectile.GetComponent<Collider>().enabled = false;
        projectile.GetComponent<Rigidbody>().useGravity = false;
        projectile.GetComponent<Renderer>().enabled = false;
        projectile.GetComponent<Rigidbody>().velocity = Vector3.zero;

        for (var i = 0; i < overlaping.Length; i++)
        {
            if (overlaping[i].gameObject.tag == "Enemy")
            {
                this.DestroyEnemy(overlaping[i].gameObject);
            }
        }

        projectile.GetComponentInChildren<ParticleSystem>().Emit(1000);
        Destroy(projectile.gameObject, 5);
    }

    private void onProjectileMouseUp(Projectile projectile)
    {
        if (!projectile.isFired) {
            GameObject.Find("ExplosiveShotBtn").GetComponent<Button>().interactable = false;

            projectile.isFired = true;

            Vector3 mouseEndPosition = Input.mousePosition;
            mouseEndPosition.z = transform.position.z - Camera.main.transform.position.z;
            mouseEndPosition = Camera.main.ScreenToWorldPoint(mouseEndPosition);

            var force = mouseEndPosition - mouseStartPosition;
            force.z = force.magnitude;

            projectile.GetComponent<Rigidbody>().AddForce(Camera.main.transform.TransformDirection(force * 100));

            projectile.onCollision += onProjectileCollision;

            Invoke("Reload", respawnTime);
            Destroy(projectile.gameObject, this.projectileLifetime);
        }
    }

    private void onProjectileMouseDown(Projectile projectile)
    {
        mouseStartPosition = Input.mousePosition;
        mouseStartPosition.z = transform.position.z - Camera.main.transform.position.z;
        mouseStartPosition = Camera.main.ScreenToWorldPoint(mouseStartPosition);
    }

    private bool canBeDestroyed(MeshRenderer mesh1, MeshRenderer mesh2)
    {
        return (mesh1.material.color == mesh2.material.color) || mesh2.material.color == Color.black;
    }

    private IEnumerator StartFog()
    {
        RenderSettings.fog = true;
        RenderSettings.fogDensity = 0.2f;

        yield return new WaitForSeconds(5f);
        RenderSettings.fog = false;
    }

    private void GameOver()
    {
        if(this.health == 0)
        {
            Application.LoadLevel(4);
        }
    }
}