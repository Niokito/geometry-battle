﻿using UnityEngine;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour
{
    [SerializeField]
    private Canvas quitMenu;
    [SerializeField]
    private Canvas startMenu;
    [SerializeField]
    private Canvas selectLevelMenu;
    [SerializeField]
    private Button startTest;
    [SerializeField]
    private Button exitText;
    [SerializeField]
    private Button selectLevelText;

    void Start()
    {
        this.quitMenu = this.quitMenu.GetComponent<Canvas>();
        this.startTest = this.startTest.GetComponent<Button>();
        this.exitText = this.exitText.GetComponent<Button>();
        this.selectLevelText = this.selectLevelText.GetComponent<Button>();
        this.quitMenu.enabled = false;
        this.selectLevelMenu.enabled = false;
    }

    public void ExitPress()
    {
        this.quitMenu.enabled = true;
        this.startTest.enabled = false;
        this.exitText.enabled = false;
    }

    public void NoPress()
    {
        this.quitMenu.enabled = false;
        this.startTest.enabled = true;
        this.exitText.enabled = true;
    }

    public void SelectLevelPress()
    {
        this.selectLevelMenu.enabled = true;
        this.quitMenu.enabled = false;
        this.startTest.enabled = false;
        this.exitText.enabled = false;
    }

    public void BackPress()
    {
        this.startMenu.enabled = true;
        this.selectLevelMenu.enabled = false;
        this.startTest.enabled = true;
        this.exitText.enabled = true;
    }

    public void StartGame()
    {
        Application.LoadLevel(1);
    }

    public void StartFirstLevel()
    {
        Application.LoadLevel(1);
    }

    public void StartSecondLevel()
    {
        Application.LoadLevel(2);
    }

    public void StartThirdLevel()
    {
        Application.LoadLevel(3);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
