﻿using UnityEngine;
using System.Collections;

public class BuffSpawn : MonoBehaviour
{
    public Transform buff;
    private float waitTime = 15f;

    void Start()
    {
        StartCoroutine("WaitAndRelocate");
    }

    void RelocateBuff()
    {
        Vector3 randomPosition = new Vector3(Random.Range(-1F, 1F), 0, Random.Range(-5F, -3F));
        GameObject.FindGameObjectWithTag("Buff").transform.position = randomPosition;
        GameObject.FindGameObjectWithTag("Buff").GetComponent<MeshRenderer>().enabled = true;
        GameObject.FindGameObjectWithTag("Buff").GetComponent<BoxCollider>().enabled = true;
    }

    IEnumerator WaitAndRelocate()
    {
        while (true)
        {
            yield return new WaitForSeconds(this.waitTime);
            RelocateBuff();
        }
    }
}
