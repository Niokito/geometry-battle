﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CubeGenerator : MonoBehaviour
{
    public string spawnLocationTag;

    public Dictionary<int, int> activeCubes = new Dictionary<int, int>()
    {
        {0, 0},
        {1, 0},
        {2, 0},
        {3, 0}
    };

    public GameObject[] Cubes;

    private GameObject[] spawnLocations;

    void Start()
    {
        this.activeCubes[0] = 0;
        this.activeCubes[1] = 0;
        this.activeCubes[2] = 0;
        this.activeCubes[3] = 0;

        this.spawnLocations = GameObject.FindGameObjectsWithTag("CubeSpawnLocation");
        StartCoroutine("spawnCubes");
    }

    private IEnumerator spawnCubes()
    {
        while (true)
        {
            for (int i = 0; i <= this.spawnLocations.Length; i++)
            {
                int location = Random.Range(0, this.spawnLocations.Length);
                int type = Random.Range(0, this.Cubes.Length);

                GameObject cube = Instantiate(Cubes[type], spawnLocations[location].transform.position, spawnLocations[location].transform.rotation) as GameObject;
                this.activeCubes[type]++;
            }

            yield return new WaitForSeconds(10);
        }
    }
}
