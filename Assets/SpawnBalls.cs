﻿using UnityEngine;
using System.Collections;

public class SpawnBalls : MonoBehaviour
{
    private Transform normalBall;

    private Transform multiShotBall;

    private Texture btnTexture;

    private Vector3 spawnPosition;
    void Start()
    {
        spawnPosition = new Vector3(-0.5f, 0.25f, -5f);
    }
    void OnGUI()
    {
        if (GUI.Button(new Rect(70, 430, 40, 40), btnTexture))
        {
            if (checkIfPosEmpty(spawnPosition))
            {
                Instantiate(normalBall, new Vector3(-0.5f, 5, -5f), Quaternion.identity);
            }
        }
        if (GUI.Button(new Rect(120, 430, 40, 40), btnTexture))
        {
            if (checkIfPosEmpty(spawnPosition))
            {
                Instantiate(multiShotBall, new Vector3(-0.5f, 5, -5f), Quaternion.identity);
            }
        }
    }

    public bool checkIfPosEmpty(Vector3 targetPos)
    {
        GameObject[] allMovableThings = GameObject.FindGameObjectsWithTag("Balls");
        foreach (GameObject current in allMovableThings)
        {
            if (current.transform.position == targetPos)
                return false;
        }
        return true;
    }
}
