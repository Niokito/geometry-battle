﻿using UnityEngine;
using System.Collections;

public class MoveCube : MonoBehaviour
{
    public float torque;
    public Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        transform.Translate(Time.deltaTime, 0, 0, Camera.main.transform);
    }
}